from django import forms
from ..archivo.models import Archivos

class ArchivosForm(forms.ModelForm):
    class Meta:
        model = Archivos
        fields = ["nombre", "descripcion", "media"]