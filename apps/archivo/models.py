# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Archivos(models.Model):
    nombre = models.CharField(max_length=120, null=False)
    descripcion = models.TextField(null=True)
    media = models.ImageField(upload_to='myfolder/', blank=False, null=False)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __unicode__(self):
        return self.nombre