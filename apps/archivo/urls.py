from django.conf.urls import url, include

from ..archivo.views import list_archivos, create_archivos, update_archivos, delete_archivos

urlpatterns = [
    url(r'^$', list_archivos, name='list_archivos'),
    url(r'^new/$', create_archivos, name='create_archivos'),
    url(r'^update/(?P<id>[0-9]+)/$', update_archivos, name='update_archivos'),
    url(r'^delete/(?P<id>[0-9]+)/$', delete_archivos, name='delete_archivos'),
]
