# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import Archivos
from .forms import ArchivosForm
from ..correspondencia.views import list_correspondencia

# Create your views here.
def list_archivos(request):
    if request.user.is_authenticated():
        archivos = Archivos.objects.all()
        return render(request, 'archivo/archivo.html', {'archivos': archivos})
    else:
        return redirect('auth_login')

def create_archivos(request):
    if request.user.is_authenticated():
        form = ArchivosForm(request.POST or None, request.FILES or None)

        context = {
            "form": form
        }

        if form.is_valid():
            instance = form.save(commit=False)
            nombre = form.cleaned_data.get("nombre")
            form.save()
            return redirect('list_archivos')

        return render(request,"archivo/archivo-form.html", context)
    else:
        return redirect('auth_login')


def update_archivos(request, id):
    if request.user.is_authenticated():
        archivo = Archivos.objects.get(id=id)
        form = ArchivosForm(request.POST or None, request.FILES or None, instance=archivo)

        if form.is_valid():
            form.save()
            return redirect('list_archivos')

        return render(request, 'archivo/archivo-form.html', {'form': form, 'archivo': archivo})
    else:
        return redirect('auth_login')

def delete_archivos(request, id):
    if request.user.is_authenticated():
        archivo = Archivos.objects.get(id=id)

        if request.method == 'POST':
            archivo.delete()
            return redirect('list_archivos')

        return render(request, 'archivo/ar-delete-confirm.html', {'archivo': archivo})
    else:
        return redirect('auth_login')