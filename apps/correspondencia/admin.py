# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Correspondencia


class AdminCorrespondencia(admin.ModelAdmin):
    list_display = ["folio", "detalles", "fecha", "asunto", "ESTADO", "remitente", "tipo_correspondencia", "departamento", "destinatario"]

admin.site.register(Correspondencia)
