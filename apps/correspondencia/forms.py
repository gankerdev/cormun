from django import forms

from ..correspondencia.models import Correspondencia


class CorrespondenciaForm(forms.ModelForm):

    class Meta:
        model = Correspondencia

        fields = [
            'folio',
            'detalle',
            
            'asunto',
            'ESTADO',
            'remitente',
            'tipo_correspondencia',
            'departamento',
            'destinatario',
        ]
        labels = {
            'folio': 'Folio',
            'detalle': 'Detalle',
            
            'asunto': 'Asunto',
            'ESTADO': 'Estado',
            'remitente': 'Remitente',
            'tipo_correspondencia': 'Tipo de correspondencia',
            'departamento': 'Departamento',
            'destinatario': 'Destinatario', 
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'detalle': forms.TextInput(attrs={'class':'form-control'}),
            
            'asunto': forms.TextInput(attrs={'class':'form-control'}),
            'ESTADO': forms.Select(attrs={'class':'form-control'}),
            'remitente': forms.Select(attrs={'class':'form-control'}),
            'tipo_correspondencia': forms.Select(attrs={'class':'form-control'}),
            'departamento': forms.Select(attrs={'class':'form-control'}),
            'destinatario': forms.Select(attrs={'class':'form-control'}),
        }
