# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ..remitente.models import Remitente
from ..tipo_correspondencia.models import Tipo_correspondencia
from ..departamento.models import Departamento
from ..destinatario.models import Destinatario

# Create your models here.
class Correspondencia(models.Model):
    folio = models.IntegerField()
    detalle = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True, auto_now=False)
    asunto = models.CharField(max_length=50)
    ESTADO_CHOICES = (
        ('R','Recibido'),
        ('E','Entregado'),
        ('V','Visado'),
        ('T','Transito'),
    )
    ESTADO = models.CharField(max_length=1, choices=ESTADO_CHOICES) 
    remitente = models.ForeignKey(Remitente, null=True, blank=True, on_delete=models.CASCADE)
    tipo_correspondencia = models.ForeignKey(Tipo_correspondencia, null=True, blank=True, on_delete=models.CASCADE)
    departamento = models.ForeignKey(Departamento, null=True, blank=True, on_delete=models.CASCADE)
    destinatario = models.ForeignKey(Destinatario, null=True, blank=True, on_delete=models.CASCADE)

    def __unicode__(self):
        return str(self.folio)

