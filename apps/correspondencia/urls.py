from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from ..correspondencia.views import list_correspondencia, create_correspondencia, update_correspondencia, delete_correspondencia, search_correspondencia, ReporteCorrespondenciaPDF

urlpatterns = [
    url(r'^$', list_correspondencia, name='list_correspondencia'),
    url(r'^new/$', create_correspondencia, name='create_correspondencia'),
    url(r'^update/(?P<id>[0-9]+)/$', update_correspondencia, name='update_correspondencia'),
    url(r'^delete/(?P<id>[0-9]+)/$', delete_correspondencia, name='delete_correspondencia'),
    url(r'^search/$', search_correspondencia, name='search_correspondencia'),
    url(r'^reporte_correspondencia_pdf/$', login_required(ReporteCorrespondenciaPDF.as_view()), name='reporte_correspondencia_pdf'),
]
