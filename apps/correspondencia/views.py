# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Correspondencia
from .forms import CorrespondenciaForm
from django.http import HttpResponse 
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Table, TableStyle, Image
from reportlab.lib.enums import TA_CENTER
from reportlab.lib import colors
from django.views.generic import View
from django.conf import settings


# Create your views here.
def list_correspondencia(request):
    if request.user.is_authenticated():
        correspondencias = Correspondencia.objects.order_by('-fecha')
        paginator = Paginator(correspondencias, 7)
        page = request.GET.get('page')
        try:
            correspondencias = paginator.page(page)
        except PageNotAnInteger:
            correspondencias = paginator.page(1)
        except EmptyPage:
            correspondencias = paginator.page(paginator.num_pages)
        return render(request, 'correspondencia/correspondencia.html', {'correspondencias': correspondencias, 'page': page})
    else:
        return redirect('auth_login')        


def create_correspondencia(request):
    if request.user.is_authenticated():
        form = CorrespondenciaForm(request.POST or None)

        if form.is_valid():
            form.save()
            return redirect('list_correspondencia')

        return render(request, 'correspondencia/correspondencia-form.html', {'form': form})
    else:
        return redirect('auth_login')

def update_correspondencia(request, id):
    if request.user.is_authenticated():
        correspondencia = Correspondencia.objects.get(id=id)
        form = CorrespondenciaForm(request.POST or None, instance=correspondencia)

        if form.is_valid():
            form.save()
            return redirect('list_correspondencia')

        return render(request, 'correspondencia/correspondencia-form.html', {'form': form, 'correspondencia': correspondencia})
    else:
        return redirect('auth_login')

def delete_correspondencia(request, id):
    if request.user.is_authenticated():
        correspondencia = Correspondencia.objects.get(id=id)

        if request.method == 'POST':
            correspondencia.delete()
            return redirect('list_correspondencia')

        return render(request, 'correspondencia/cor-delete-confirm.html', {'correspondencia': correspondencia})
    else:
        return redirect('auth_login')

        
def search_correspondencia(request):
    if request.user.is_authenticated():
        buscar = request.POST['buscar']
        correspondencias = Correspondencia.objects.filter(folio__contains=buscar)
        return render (request, 'correspondencia/buscar.html', {'correspondencias':correspondencias})
    else:
        return redirect('auth_login')


class ReporteCorrespondenciaPDF(View):
    def cabecera(self,pdf):
        archivo_imagen = settings.STATIC_ROOT+'/img/escudo.png'
        pdf.drawImage(archivo_imagen, 50, 740, 120, 90, preserveAspectRatio=True)
        pdf.setFont("Helvetica", 16)
        pdf.drawString(270, 790, u"CorMun")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"Reporte de correspondencias")

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        buffer = BytesIO()
        pdf = canvas.Canvas(buffer)
        self.cabecera(pdf)
        y = 510
        self.tabla(pdf, y)
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response

    def tabla(self, pdf, y):
        encabezados = ('Folio', 'Asunto', 'Estado', 'Remitente', 'Departamento', 'Destinatario')
        detalles = [(cor.folio, cor.asunto, cor.ESTADO, cor.remitente, cor.departamento, cor.destinatario)for cor in Correspondencia.objects.all()]
        detalle_orden = Table([encabezados] + detalles, colWidths=[2 * cm, 3 * cm, 1.4 * cm, 3 * cm, 3 * cm, 3 * cm])
        detalle_orden.setStyle(TableStyle(
        [   
            ('ALIGN', (0,0),(3,0), 'CENTER'),
            ('GRID', (0,0), (-1,-1), 1, colors.black),
            ('FONTSIZE', (0,0), (-1,-1), 10),
            ]
        ))
        detalle_orden.wrapOn(pdf, 800, 600)
        detalle_orden.drawOn(pdf, 60, y)