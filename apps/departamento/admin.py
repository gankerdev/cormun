# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Departamento


class AdminDepartamento(admin.ModelAdmin):
    list_display = ["nombre", "encargado", "user"]

admin.site.register(Departamento)
