from django import forms

from ..departamento.models import Departamento


class DepartamentoForm(forms.ModelForm):

    class Meta:
        model = Departamento

        fields = [
            'nombre',
            'encargado',
            'user',
        ]
        labels = {
            'nombre': 'Nombre',
            'encargado': 'Encargado',
            'user': 'Usuario:',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'encargado': forms.TextInput(attrs={'class':'form-control'}),
            'user': forms.Select(attrs={'class':'form-control'}),
        }
