# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Departamento(models.Model):
    nombre = models.CharField(max_length=50)
    encargado = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    def __unicode__(self):
        return self.nombre