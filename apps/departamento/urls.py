from django.conf.urls import url, include

from ..departamento.views import list_departamento, create_departamento, update_departamento, delete_departamento

urlpatterns = [
    url(r'^$', list_departamento, name='list_departamento'),
    url(r'^new/$', create_departamento, name='create_departamento'),
    url(r'^update/(?P<id>[0-9]+)/$', update_departamento, name='update_departamento'),
    url(r'^delete/(?P<id>[0-9]+)/$', delete_departamento, name='delete_departamento'),
]
