# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import Departamento
from .forms import DepartamentoForm

# Create your views here.
def list_departamento(request):
    if request.user.is_authenticated():
        departamentos = Departamento.objects.all()
        return render(request, 'departamento/departamento.html', {'departamentos': departamentos})
    else:
        return redirect('auth_login')

def create_departamento(request):
    if request.user.is_authenticated():
        form = DepartamentoForm(request.POST or None)

        if form.is_valid():
            form.save()
            return redirect('list_departamento')

        return render(request, 'departamento/departamento-form.html', {'form': form})
    else:
        return redirect('auth_login')

def update_departamento(request, id):
    if request.user.is_authenticated():
        departamento = Departamento.objects.get(id=id)
        form = DepartamentoForm(request.POST or None, instance=departamento)

        if form.is_valid():
            form.save()
            return redirect('list_departamento')

        return render(request, 'departamento/departamento-form.html', {'form': form, 'departamento': departamento})
    else:
        return redirect('auth_login')

def delete_departamento(request, id):
    if request.user.is_authenticated():
        departamento = Departamento.objects.get(id=id)

        if request.method == 'POST':
            departamento.delete()
            return redirect('list_departamento')

        return render(request, 'departamento/dep-delete-confirm.html', {'departamento': departamento})
    else:
        return redirect('auth_login')