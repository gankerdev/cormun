# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Destinatario


class AdminDestinatario(admin.ModelAdmin):
    list_display = ["nombre", "apellido", "telefono", "email", "cargo", "direccion"]


admin.site.register(Destinatario)