from django import forms

from ..destinatario.models import Destinatario


class DestinatarioForm(forms.ModelForm):

    class Meta:
        model = Destinatario

        fields = [
            'nombre',
            'apellido',
            'telefono',
            'email',
            'cargo',
            'direccion',
        ]
        labels = {
            'nombre': 'Nombre',
            'apellido': 'Apellido',
            'telefono': 'Telefono',
            'email': 'Email',
            'cargo': 'Cargo',
            'direccion': 'Direccion',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'apellido': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control'}),
            'cargo': forms.TextInput(attrs={'class':'form-control'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
        }
