# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Destinatario(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    telefono = models.IntegerField()
    email = models.EmailField()
    cargo = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)

    def __unicode__(self):
        return self.nombre
