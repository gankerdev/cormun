from django.conf.urls import url, include

from ..destinatario.views import list_destinatario, create_destinatario, update_destinatario, delete_destinatario

urlpatterns = [
    url(r'^$', list_destinatario, name='list_destinatario'),
    url(r'^new/$', create_destinatario, name='create_destinatario'),
    url(r'^update/(?P<id>[0-9]+)/$', update_destinatario, name='update_destinatario'),
    url(r'^delete/(?P<id>[0-9]+)/$', delete_destinatario, name='delete_destinatario'),
]
