# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import Destinatario
from .forms import DestinatarioForm

# Create your views here.
def list_destinatario(request):
    if request.user.is_authenticated():
        destinatarios = Destinatario.objects.all()
        return render(request, 'destinatario/destinatario.html', {'destinatarios': destinatarios})
    else:
        return redirect('auth_login')

def create_destinatario(request):
    if request.user.is_authenticated():
        form = DestinatarioForm(request.POST or None)

        if form.is_valid():
            form.save()
            return redirect('list_destinatario')

        return render(request, 'destinatario/destinatario-form.html', {'form': form})
    else:
        return redirect('auth_login')

def update_destinatario(request, id):
    if request.user.is_authenticated():
        destinatario = Destinatario.objects.get(id=id)
        form = DestinatarioForm(request.POST or None, instance=destinatario)

        if form.is_valid():
            form.save()
            return redirect('list_destinatario')

        return render(request, 'destinatario/destinatario-form.html', {'form': form, 'destinatario': destinatario})
    else:
        return redirect('auth_login')

def delete_destinatario(request, id):
    if request.user.is_authenticated():    
        destinatario = Destinatario.objects.get(id=id)

        if request.method == 'POST':
            destinatario.delete()
            return redirect('list_destinatario')

        return render(request, 'destinatario/des-delete-confirm.html', {'destinatario': destinatario})
    else:
        return redirect('auth_login')