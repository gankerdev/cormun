# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Remitente


class AdminRemitente(admin.ModelAdmin):
    list_display = ["nombre","telefono","direccion","email"]


admin.site.register(Remitente)   