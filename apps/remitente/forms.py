from django import forms

from ..remitente.models import Remitente


class RemitenteForm(forms.ModelForm):
    class Meta:
        model = Remitente

        fields = [
            'nombre',
            'telefono',
            'direccion',
            'email',
        ]
        labels = {
            'nombre': 'Nombre',
            'telefono': 'Telefono',
            'direccion': 'Direccion',
            'email': 'Email',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control'}),
        }
