from django.conf.urls import url, include

from ..remitente.views import list_remitente, create_remitente, update_remitente, delete_remitente

urlpatterns = [
    url(r'^$', list_remitente, name='list_remitente'),
    url(r'^new/$', create_remitente, name='create_remitente'),
    url(r'^update/(?P<id>[0-9]+)/$', update_remitente, name='update_remitente'),
    url(r'^delete/(?P<id>[0-9]+)/$', delete_remitente, name='delete_remitente'),
]
