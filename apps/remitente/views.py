# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import Remitente
from .forms import RemitenteForm

# Create your views here.
def list_remitente(request):
    if request.user.is_authenticated():
        remitentes = Remitente.objects.all()
        return render(request, 'remitente/remitente.html', {'remitentes': remitentes})
    else:
        return redirect('auth_login')

def create_remitente(request):
    if request.user.is_authenticated():    
        form = RemitenteForm(request.POST or None)

        if form.is_valid():
            form.save()
            return redirect('list_remitente')

        return render(request, 'remitente/remitente-form.html', {'form': form})
    else:
        return redirect('auth_login')

def update_remitente(request, id):
    if request.user.is_authenticated():
        remitente = Remitente.objects.get(id=id)
        form = RemitenteForm(request.POST or None, instance=remitente)

        if form.is_valid():
            form.save()
            return redirect('list_remitente')

        return render(request, 'remitente/remitente-form.html', {'form': form, 'remitente': remitente})
    else:
        return redirect('auth_login')

def delete_remitente(request, id):
    if request.user.is_authenticated():
        remitente = Remitente.objects.get(id=id)

        if request.method == 'POST':
            remitente.delete()
            return redirect('list_remitente')

        return render(request, 'remitente/rem-delete-confirm.html', {'remitente': remitente})
    else:
        return redirect('auth_login')