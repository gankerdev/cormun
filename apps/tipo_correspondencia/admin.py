# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Tipo_correspondencia


class AdminTipo_correspondencia(admin.ModelAdmin):
    list_display = ["nombre", "detalle"]


admin.site.register(Tipo_correspondencia)