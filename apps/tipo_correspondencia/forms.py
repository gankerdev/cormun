from django import forms

from ..tipo_correspondencia.models import Tipo_correspondencia


class TipoCorrespondenciaForm(forms.ModelForm):
    class Meta:
        model = Tipo_correspondencia

        fields = [
            'nombre',
            'detalle',
        ]
        labels = {
            'nombre': 'Nombre',
            'detalle': 'Detalle',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'detalle': forms.Textarea(attrs={'class':'form-control'}),
        }
