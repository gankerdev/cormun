from django.conf.urls import url, include

from ..tipo_correspondencia.views import list_tipo_correspondencia, create_tipo_correspondencia, update_tipo_correspondencia, delete_tipo_correspondencia

urlpatterns = [
    url(r'^$', list_tipo_correspondencia, name='list_tipo_correspondencia'),
    url(r'^new/$', create_tipo_correspondencia, name='create_tipo_correspondencia'),
    url(r'^update/(?P<id>[0-9]+)/$', update_tipo_correspondencia, name='update_tipo_correspondencia'),
    url(r'^delete/(?P<id>[0-9]+)/$', delete_tipo_correspondencia, name='delete_tipo_correspondencia'),
]