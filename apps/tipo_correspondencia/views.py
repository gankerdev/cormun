# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import Tipo_correspondencia
from .forms import TipoCorrespondenciaForm

# Create your views here.
def list_tipo_correspondencia(request):
    if request.user.is_authenticated():
        tipo_correspondencias = Tipo_correspondencia.objects.all()
        return render(request, 'tipo_correspondencia/tipo_correspondencia.html', {'tipo_correspondencias': tipo_correspondencias})
    else:
        return redirect('auth_login')

def create_tipo_correspondencia(request):
    if request.user.is_authenticated():        
        form = TipoCorrespondenciaForm(request.POST or None)

        if form.is_valid():
            form.save()
            return redirect('list_tipo_correspondencia')

        return render(request, 'tipo_correspondencia/tipo_correspondencia-form.html', {'form': form})
    else:
        return redirect('auth_login')


def update_tipo_correspondencia(request, id):
    if request.user.is_authenticated():
        tipo_correspondencia = Tipo_correspondencia.objects.get(id=id)
        form = TipoCorrespondenciaForm(request.POST or None, instance=tipo_correspondencia)

        if form.is_valid():
            form.save()
            return redirect('list_tipo_correspondencia')

        return render(request, 'tipo_correspondencia/tipo_correspondencia-form.html', {'form': form, 'tipo_correspondencia': tipo_correspondencia})
    else:
        return redirect('auth_login')

def delete_tipo_correspondencia(request, id):
    if request.user.is_authenticated():
        tipo_correspondencia = Tipo_correspondencia.objects.get(id=id)

        if request.method == 'POST':
            tipo_correspondencia.delete()
            return redirect('list_tipo_correspondencia')

        return render(request, 'tipo_correspondencia/tipo-delete-confirm.html', {'tipo_correspondencia': tipo_correspondencia})
    else:
        return redirect('auth_login')