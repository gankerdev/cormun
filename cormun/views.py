from django.shortcuts import render, redirect
from apps.correspondencia.models import Correspondencia

def home(request):
    if request.user.is_authenticated:
        return render(request, "base/home.html", {})
    else:
        return redirect('auth_login')


def perfil(request):
    if request.user.is_authenticated:
        return render(request, "base/perfil.html", {})
    else:
        return redirect('auth_login')
